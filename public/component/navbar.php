<nav class="border-gray-200 px-4 sm:px-4 rounded fixed w-full z-20 top-0 left-0 transition-all">
    <div class="lg:max-w-[970px] lg:w-full lg:m-auto">

        <div class="container flex flex-wrap items-center justify-between mx-auto bg-nav rounded-b-xl" style="background-color:#1E1E1E;">
            <a href="index.php" class="flex items-center">
                <img src="assets/img/navbar/logo.svg" class="max-w-[150px] h-auto block w-full m-auto" alt="img" width="246" height="93">
            </a>
            <div class="flex md:order-2">

                <button data-collapse-toggle="navbar-sticky" type="button"
                    class="inline-flex items-center p-2 text-sm  text-white button-hamburger border border-white rounded-lg md:hidden focus:outline-none focus:ring-2 focus:ring-gray-200 "
                    aria-controls="navbar-sticky" aria-expanded="false">
                    <span class="sr-only">Open main menu</span>
                    <svg class="w-6 h-6" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                            clip-rule="evenodd"></path>
                    </svg>
                </button>
            </div>
            <div class="items-center justify-between hidden w-full md:flex md:w-auto md:order-1" id="navbar-sticky">
                <ul
                    class="konten-nav bg-black bg-opacity-[0.9] sm:!bg-transparent font-semibold mt-3 text-white flex flex-col  rounded-lg  md:flex-row md:space-x-8 xl:space-x-12 md:mt-0 md:text-sm md:font-bold md:border-0 ">
                    <li>
                        <a href="#header"
                            class="block py-2 pl-3 pr-4 text-nav rounded hover:bg-gray-400 md:hover:bg-transparent md:hover:text-blue-700 md:p-0"
                            aria-current="page">HOME</a>
                    </li>
                    <li>
                        <a href="#tentang"
                            class="block py-2 pl-3 pr-4 text-nav rounded hover:bg-gray-400 md:hover:bg-transparent md:hover:text-blue-700 md:p-0 ">TENTANG</a>
                    </li>
                    <li>
                        <a href="#tema"
                            class="block py-2 pl-3 pr-4 text-nav rounded hover:bg-gray-400 md:hover:bg-transparent md:hover:text-blue-700 md:p-0">TEMA</a>
                    </li>
                    <li>
                        <a href="#hadiah"
                            class="block py-2 pl-3 pr-4 text-nav rounded hover:bg-gray-400 md:hover:bg-transparent md:hover:text-blue-700 md:p-0">HADIAH</a>
                    </li>
                    <li>
                        <a href="#juri"
                            class="block py-2 pl-3 pr-4 text-nav rounded hover:bg-gray-400 md:hover:bg-transparent md:hover:text-blue-700 md:p-0">JURI</a>
                    </li>
                    <li>
                        <a href="#syarat"
                            class="block py-2 pl-3 pr-4 text-nav rounded hover:bg-gray-400 md:hover:bg-transparent md:hover:text-blue-700 md:p-0">SYARAT & KETENTUAN</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>

<script>
$(window).scroll(function() {
    if ($(window).scrollTop() >= 100) {
        // scroll
        $('nav').css('background', 'white');
        $('.bg-nav').css('background', 'transparent');
        $('.konten-nav').css('background-color', 'transparent');
        $('.text-nav').css('color', '#494652');
        $('.button-hamburger').css('color', 'black');
        $('.button-hamburger').css('border', '1px solid #000');
    } else {
        // top
        $('nav').css('background', 'transparent');
        $('.bg-nav').css('background', '#1e1e1e');
        $('.konten-nav').css('background-color', 'rgba(0, 0, 0, 0.9)');
        $('.text-nav').css('color', 'white');
        $('.button-hamburger').css('color', 'white');
        $('.button-hamburger').css('border', '1px solid #fff');
    }
});
</script>
