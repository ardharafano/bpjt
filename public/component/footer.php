<div id="footer" class="bg-[#131882] text-white">
    <div class="lg:max-w-[970px] lg:w-full lg:m-auto p-12">
        <div class="flex flex-wrap justify-center items-center mx-auto">

            <div class="xl:w-6/12 lg:text-start text-center">
                <div>Hak Cipta © 2023 Kementerian Pekerjaan Umum dan Perumahan Rakyat Republik Indonesia. All Rights
                    Reserved</div>
            </div>

            <div class="xl:w-6/12 mt-5 lg:mt-1">
                <div class="flex flex-wrap justify-end">
                    <a href="https://www.facebook.com/KemenPUPR" target="_blank">
                        <img src="assets/img/footer/fb.svg?v=1" class="w-full block m-auto max-w-[40px] h-[auto] pr-1"
                            alt="img" width="40" height="40">
                    </a>

                    <a href="https://twitter.com/kemenPU" target="_blank">
                        <img src="assets/img/footer/twt.svg?v=1" class="w-full block m-auto max-w-[40px] h-[auto] pr-1"
                            alt="img" width="40" height="40">
                    </a>

                    <a href="https://www.youtube.com/user/kemenPU" target="_blank">
                        <img src="assets/img/footer/yt.svg?v=1" class="w-full block m-auto max-w-[40px] h-[auto] pr-1"
                            alt="img" width="40" height="40">
                    </a>

                    <a href="https://www.instagram.com/kemenpupr/" target="_blank">
                        <img src="assets/img/footer/ig.svg?v=1" class="w-full block m-auto max-w-[40px] h-[auto] pr-1" alt="img"
                            width="40" height="40">
                    </a>
                </div>

            </div>

        </div>
    </div>
</div>