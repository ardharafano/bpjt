<div id="hadiah"
    class="flex justify-center items-center bg-[url('../img/hadiah/bg.svg?v=1')] bg-cover bg-no-repeat bg-center w-full md:min-h-[150vh] min-h-[170vh] p-4">

    <div class="lg:max-w-[970px] lg:w-full lg:m-auto hs-init hs-inview" id="fade-up-card" data-hs="fade up">
        <h1
            class="text-center mt-[100px] md:mt-0 text-[#fff] font-bold text-3xl border-b-8 border-b-[#FDB910] w-full max-w-[170px] m-auto">
            HADIAH</h1>
        <div class="flex flex-wrap">
            <div class="w-full">
                <h1 class="text-[#FDB910] mt-12 font-bold text-xl">Aerial Fotografi</h1>
                <div class="bg-white my-5 p-5 rounded-xl">

                    <div class="flex flex-wrap justify-center items-center">
                        <div class="lg:w-6/12 mb-10 sm:mb-0 sm:pr-4 pr-0">
                            <img src="assets/img/hadiah/hadiah-1.svg" alt="img" width="129" height="107"
                                class="float-none lg:float-left block w-full m-auto max-w-[129px] h-auto pr-4">
                            <ul class="mt-5 sm:mt-0">
                                <li>Juara 1: Rp7.000.000 + e-sertifikat</li>
                                <li>Juara 2: Rp3.000.000 + e-sertifikat</li>
                                <li>Juara 3: Rp2.000.000 + e-sertifikat</li>
                            </ul>
                        </div>
                        <div class="lg:w-6/12">
                            <img src="assets/img/hadiah/hadiah-2.svg" alt="img" width="129" height="107"
                                class="float-none lg:float-left block w-full m-auto max-w-[129px] h-auto pr-4">
                            <ul>
                                <li class="w-full max-w-[250px] lg:max-w-full mx-auto mt-5 sm:mt-0">10 Juara Harapan:
                                    Masing-masing pemenang mendapatkan E-wallet @Rp500.000 +
                                    e-sertifikat</li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="flex flex-wrap">
            <div class="w-full">
                <h1 class="text-[#FDB910] mt-5 font-bold text-xl">Non Aerial Fotografi</h1>
                <div class="bg-white my-5 p-5 rounded-xl">

                    <div class="flex flex-wrap justify-center items-center">
                        <div class="lg:w-6/12 mb-10 sm:mb-0 sm:pr-4 pr-0">
                            <img src="assets/img/hadiah/hadiah-1.svg" alt="img" width="129" height="107"
                                class="float-none lg:float-left block w-full m-auto max-w-[129px] h-auto pr-4">
                            <ul class="mt-5 sm:mt-0">
                                <li>Juara 1: Rp7.000.000 + e-sertifikat</li>
                                <li>Juara 2: Rp3.000.000 + e-sertifikat</li>
                                <li>Juara 3: Rp2.000.000 + e-sertifikat</li>
                            </ul>
                        </div>
                        <div class="lg:w-6/12">
                            <img src="assets/img/hadiah/hadiah-2.svg" alt="img" width="129" height="107"
                                class="float-none lg:float-left block w-full m-auto max-w-[129px] h-auto pr-4">
                            <ul>
                                <li class="w-full max-w-[250px] lg:max-w-full mx-auto mt-5 sm:mt-0">10 Juara Harapan:
                                    Masing-masing pemenang mendapatkan E-wallet @Rp500.000 +
                                    e-sertifikat</li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>