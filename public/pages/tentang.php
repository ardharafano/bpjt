<div id="tentang"
    class="bg-[url('../img/tentang/bg.svg')] bg-cover bg-no-repeat bg-center w-full h-[650px] p-4 relative top-[-70px] lg:top-[-80px] -z-10">

    <div class="lg:max-w-[970px] lg:w-full lg:m-auto hs-init hs-inview" id="fade-up-card" data-hs="fade up">
        <h1
            class="text-center mt-32 text-[#0A163A] font-bold text-3xl border-b-8 border-b-[#FDB910] w-full max-w-[170px] m-auto">
            TENTANG</h1>
        <div class="flex flex-wrap">
            <div class="max-w-[848px] w-full m-auto text-center text-xl">
                <p class="my-5">Pembangunan infrastruktur jalan tol yang memberikan manfaat bagi masyarakat terus
                    dilakukan oleh BPJT. Selain peningkatan konektivitas antar wilayah, hadirnya rest area yang inovatif
                    mendukung pertumbuhan ekonomi. </p>
                <p class="my-5">Semua itu dilakukan dalam <b>#ProgresNyataMembangunNegeri</b> untuk Indonesia!</p>
            </div>
        </div>
    </div>

</div>