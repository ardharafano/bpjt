<div id="juri" class="p-4">

    <div class="lg:max-w-[970px] lg:w-full lg:m-auto hs-init hs-inview" id="fade-up-card" data-hs="fade up">
        <h1
            class="text-center md:mt-0 mt-0 text-[#0A163A] font-bold text-3xl border-b-8 border-b-[#FDB910] w-full max-w-[170px] m-auto">
            JURI</h1>
        <div class="flex flex-wrap justify-center mt-24 text-xl items-center leading-relaxed">
        <div class="lg:w-4/12 pl-0 sm:pl-8 mt-5 sm:mt-0">
                <img src="assets/img/juri/pupr.svg?v=1" alt="img" width="245" height="264"
                    class="block w-full max-w-[245px] h-auto m=auto">
                <div class="w-full max-w-[245px] m=auto text-center text-[#0A163A] mt-5">
                    <div class="font-bold">Perwakilan PUPR</div>
                    <!-- <div class="text-lg">Dirjen BPJT PUPR</div> -->
                </div>
            </div>
            <div class="lg:w-4/12 pl-0 sm:pl-8 mt-5 sm:mt-0">
                <img src="assets/img/juri/beawiharta.svg" alt="img" width="245" height="264"
                    class="block w-full max-w-[245px] h-auto m=auto">
                <div class="w-full max-w-[245px] m=auto text-center text-[#0A163A] mt-5">
                    <div class="font-bold">Beawiharta</div>
                    <div class="text-lg">Fotografer & Visual Storyteller</div>
                </div>
            </div>
            <div class="lg:w-4/12 pl-0 sm:pl-8 mt-5 sm:mt-0">
                <img src="assets/img/juri/ulet-infansasti.svg" alt="img" width="245" height="264"
                    class="block w-full max-w-[245px] h-auto m=auto">
                <div class="w-full max-w-[245px] m=auto text-center text-[#0A163A] mt-5">
                    <div class="font-bold">Ulet Infansasti</div>
                    <div class="text-lg">Fotografer</div>
                </div>
            </div>
        </div>
    </div>

</div>