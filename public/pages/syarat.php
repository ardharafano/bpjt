<div id="syarat"
    class="flex items-start bg-[url('../img/syarat/bg.svg')] bg-cover bg-no-repeat bg-center w-full min-h-[190vh] p-4">

    <div class="lg:max-w-[970px] lg:w-full lg:m-auto hs-init hs-inview" id="fade-up-card" data-hs="fade up">
        <h1
            class="text-center lg:-mt-0 mt-52 text-[#0A163A] font-bold text-3xl border-b-8 border-b-[#FDB910] w-full max-w-[350px] m-auto">
            SYARAT & KETENTUAN</h1>
        <div class="flex flex-wrap">
            <div class="w-full text-xl mt-10 leading-relaxed">
                <ul class="list-decimal pl-6">
                    <li>Kompetisi ini gratis dan terbuka untuk umum (individu) kepada WNI dengan <b>KTP</b> dan
                        <b>SIM</b> yang masih
                        berlaku
                    </li>
                    <li>Wajib follow akun Instagram dan tiktok <b>@pupr_bpjt</b></li>
                    <li>Upload foto sesuai tema dan kategori yang diikuti</li>
                    <li>Foto yang dilombakan di-upload dalam akun media sosial Instagram pribadi dan tidak di-private
                        selama periode lomba</li>
                    <li>Foto bukan hasil manipulasi digital, penyuntingan hanya sebatas cropping, contrast, dan
                        saturation.
                        Tidak boleh ada <span class="italic"><b>watermark</b></span> atau identitas foto.</li>
                    <li>Mention dan tag akun IG <b>@pupr_bpjt</b> dengan Hashtag <b>#18TahunBPJTMenyambungNegeri
                            #SigapMembangunNegeri #HUTBPJT <span class="italic">#kategorifoto</span> (#AerialFotografi
                            atau #NonAerialFotografi)</b></li>
                    <li>Peserta diwajibkan mengisi form pendaftaran di <a href="https://linktr.ee/lombafotohutbpjt2023"
                            target="_blank">https://linktr.ee/lombafotohutbpjt2023</a>, dan
                        mengunggah foto dengan resolusi HD, link IG post dari foto yang dilombakan, serta screenshot
                        bukti follow Instagram dan tiktok <b>@pupr_bpjt</b></li>
                    <li>Foto yang akan dilombakan merupakan karya sendiri (bukan karya atau capture dari orang lain)
                        serta belum pernah dilombakan pada kompetisi manapun</li>
                    <li>Boleh mengirimkan maksimal <b>tiga foto</b> dalam <b>tiga postingan</b> (Bukan carousel)</li>
                    <li>Foto harus sesuai dengan norma-norma sosial serta tidak mengandung unsur kekerasan, pornografi,
                        penghinaan ataupun pelecehan, tidak boleh melanggar Hak Cipta, <b>SARA</b> dan <b>politik</b>.
                    </li>
                    <li>Periode posting dari <b>29 Mei</b> sampai <b>21 Juni 2023</b> pukul 23.59 WIB</li>
                    <li>Periode pengumuman pada tanggal <b>28 Juni 2023</b> di akun Instagram <b>@pupr_bpjt</b></li>
                    <li>Hak cipta foto tetap pada fotografer namun pihak BPJT berhak menggunakan foto pemenang dan
                        finalis harapan lomba untuk kepentingan publikasi dan pameran dalam kurun waktu tidak terbatas.
                    </li>
                    <li>Dengan mengirimkan foto, peserta dianggap menyetujui semua persyaratan yang ada.</li>
                    <li>Dewan juri dan panitia berhak mendiskualifikasi foto peserta sebelum atau sesudah penjurian
                        apabila tidak memenuhi semua persyaratan yang telah ditentukan.</li>
                    <li>Keputusan dewan juri tidak dapat diganggu gugat</li>
                </ul>
            </div>
        </div>
    </div>

</div>