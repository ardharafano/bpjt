<div id="tema" class="p-4">

    <div class="lg:max-w-[970px] lg:w-full lg:m-auto hs-init hs-inview" id="fade-up-card" data-hs="fade up">
        <h1
            class="text-center md:-mt-[280px] -mt-44 text-[#0A163A] font-bold text-3xl border-b-8 border-b-[#FDB910] w-full max-w-[170px] m-auto">
            TEMA</h1>
        <div class="flex flex-wrap mt-5 text-xl items-center leading-relaxed">
            <div class="lg:w-6/12 pr-0 sm:pr-4">
                <img src="assets/img/tema/aerial-fotografi.svg" width="503" height="156"
                    class="block max-w-[400px] w-full my-3 lg:mx-0 mx-auto">
                <h2 class="text-[#0A163A] font-bold mt-10">Aerial Fotografi</h2>
                <ul class="list-disc pl-6">
                    <li>Tema: <span class="italic">Landscape</span> Jalan Tol</li>
                    <li>Foto harus tajam, tidak <span class="italic">shaky</span>
                        maupun <span class="italic">blurry</span></li>
                    <li>Setiap peserta diperbolehkan meng-<span class="italic">upload</span> maksimal <b>3</b> foto</li>
                    <li>Foto harus mempunyai 3 unsur utama:</li>
                    <li class="list-none font-bold">- Objek Jalan Tol</li>
                    <li class="list-none font-bold">- Objek Aktivitas di Jalan Tol</li>
                    <li class="list-none font-bold">- Objek Alami</li>
                    <li>Kualitas foto yang dilombakan harus kualitas High Resolution dengan sisi terpendek <b>2000
                            <span class="italic">pixel</span></b>
                        dan besar <span class="italic">file</span> tidak kurang dari <b>3MB</b> (tiga megabite)</li>
                    <li>Perbaikan foto hanya diizinkan sebatas perbaikan <span class="italic">brightness/contrast,
                            level, hue/saturation</span> dan
                        <span class="italic">cropping</span> maksimal <b>20%</b>
                    </li>
                    <li><span class="italic">Hashtag</span><br><b>
                            #18TahunBPJTMenyambungNegeri<br>
                            #SigapMembangunNegeri<br>
                            #HUTBPJT<br>
                            #NonAerialFotografi</b></li>
                </ul>
            </div>
            <div class="lg:w-6/12">
                <img src="assets/img/tema/non-aerial.svg" width="503" height="156"
                    class="block max-w-[400px] w-full mt-16 mb-3 lg:mx-0 mx-auto">
                <h2 class="text-[#0A163A] font-bold mt-12">Non Aerial Fotografi</h2>
                <ul class="list-disc pl-6">
                    <li>Tema : <span class="italic">Human Interest</span>/Aktivitas di Jalan Tol</li>
                    <li>Foto harus tajam, tidak <span class="italic">shaky</span>
                        maupun <span class="italic">blurry</span></li>
                    <li>Setiap peserta diperbolehkan meng-<span class="italic">upload</span> maksimal <b>3</b> foto</li>
                    <li>Foto harus mempunyai 3 unsur utama:</li>
                    <li class="list-none font-bold">- Fasilitas dan manfaat <span class="italic">Rest Area</span></li>
                    <li class="list-none font-bold">- Kegiatan UMKM di <span class="italic">Rest Area</span></li>
                    <li class="list-none font-bold">- Beautifikasi di Jalan Tol</li>
                    <li>Peserta hanya diperbolehkan menggunakan kamera digital dan <b>DSLR</b> dengan ketentuan
                        berformat <b>JPEG</b>
                        dengan resulusi min <b>300dpi</b>, sisi terpanjang min <b>2000 <span
                                class="italic">pixel</span></b></li>
                    <li>Perbaikan foto hanya diizinkan sebatas perbaikan <span class="italic">brightness/contrast,
                            level, hue/saturation</span> dan
                        <span class="italic">cropping</span> maksimal <b>20%</b>
                    </li>
                    <li><span class="italic">Hashtag</span><br><b>
                            #18TahunBPJTMenyambungNegeri<br>
                            #SigapMembangunNegeri<br>
                            #HUTBPJT<br>
                            #NonAerialFotografi</b></li>
                </ul>
            </div>
        </div>
    </div>

</div>