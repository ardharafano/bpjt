<div id="header"
    class="flex justify-center items-center bg-[url('../img/header/bg.svg?v=2')] bg-cover bg-no-repeat bg-center w-full md:min-h-[130vh] min-h-screen px-4">
    <div class="lg:max-w-[970px] lg:w-full lg:m-auto">
        <div class="flex flex-wrap">
            <div class="w-full">
                <img src="assets/img/header/hut-bpjt.svg?v=1" alt="img" width="975" height="263"
                    class="block w-full max-w-[975px] h-auto m-auto">
                <a href="https://linktr.ee/lombafotohutbpjt2023" target="_blank">
                    <div class="bg-[#FDB910] text-center py-5 font-bold rounded-full w-full max-w-[250px] mt-5 mx-auto">
                        DAFTAR
                    </div>
                </a>
            </div>

        </div>
    </div>
</div>