/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ['./public/assets/**/*.{html,js}',
        './public/component/**/*',
        './public/pages/**/*',
        './public/index.php'
    ],
    theme: {
        extend: {},
        fontFamily: {
            sans: [
                "Inter var, sans-serif",
            ],
        },
    },
    plugins: [],
}